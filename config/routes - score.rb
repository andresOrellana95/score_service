Rails.application.routes.draw do
  resources :order_details, :only => [:index, :show, :update_score, :update_commentary]
  #Custom routes creation
  patch "valuations/score_update/:id", to: "order_details#update_score"
  patch "valuations/commentary_update/:id", to: "order_details#update_commentary"
  resources :products, :only => [:index, :show]
end
