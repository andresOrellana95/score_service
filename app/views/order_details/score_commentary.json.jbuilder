json.array! @order_detail do |od|
  #if the comment or score has been saved by the client
  if od.commentary.blank? == false || od.score != 0
    #Looking for relational data
    order = Order.find(od.order_id)
    participant = Participant.find(order.client_id)
    #Creation of the structure
    json.order order.id
    json.order_detail od.id
    json.first_name participant.first_name
    json.last_name participant.last_name
    json.score od.score
    json.commentary od.commentary
  end
end
